﻿using System;
using Microsoft.AspNetCore.Mvc;
using services.DTO;

namespace services.Controllers
{
    [Route("api/authenticate")]
    public class AuthenticateController: Controller
    {
        
        [HttpPost]
        public IActionResult Post([FromBody] AuthenticateDTO credentials)
        {
            return new ObjectResult(new UserDTO("maximilian@test.com"));
        }
    }
}
