﻿using System;
using Microsoft.AspNetCore.Mvc;
using services.DTO;
using services.Services;

namespace services.Controllers
{   
    [Route("api/forgot")]
    public class ForgotController : Controller
    {
        private IRecoveryService recoveryService;
        private IUsersService usersService;

        public ForgotController(IRecoveryService recoveryService, IUsersService usersService)
        {
            this.recoveryService = recoveryService;
            this.usersService = usersService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] ForgotDTO forgotDetails)
        {
            var user = usersService.FindByEmail(forgotDetails.Email);
            if (user != null) {
                recoveryService.CreateRecovery(user);
            }

            return new ObjectResult(new EmptyDTO());
        }
    }
}
