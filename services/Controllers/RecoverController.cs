﻿using System;
using Microsoft.AspNetCore.Mvc;
using services.DTO;
using services.Services;

namespace services.Controllers
{
    [Route("api/recover")]
    public class RecoverController : Controller
    {

        private IRecoveryService recoveryService;
        private IUsersService usersService;

        public RecoverController(IRecoveryService recoveryService, IUsersService usersService)
        {
            this.recoveryService = recoveryService;
            this.usersService = usersService;
        }


        [HttpPost]
        public IActionResult Post([FromBody] RecoverDTO forgotDetails)
        {
            var isValidToken = recoveryService.TokenExists(forgotDetails.Token);

            if (isValidToken) {
                var token = recoveryService.findByToken(forgotDetails.Token);
                var userId = token.UserId;
                usersService.UpdatePassword(userId, forgotDetails.Password);
            }

            return new ObjectResult(new EmptyDTO());
        }
    }
}
