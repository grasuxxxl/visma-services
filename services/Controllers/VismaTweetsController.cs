﻿using System;
using Microsoft.AspNetCore.Mvc;
using services.DTO;
using services.Services;

namespace services.Controllers
{
    [Route("api/tweets/search")]
    public class VismaTweetsController : Controller
    {
        public string GetVismaTweets([FromQuery] string query, [FromQuery(Name = "max_id")] string maxId) 
        {
            var twitterClient = new VismaTwitterClient();
            return twitterClient.Search(query, maxId);
        }
    }
}
