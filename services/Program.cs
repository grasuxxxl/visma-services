﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Net;
using services.Services;

namespace services
{
    class Program
    {
        public static void Main(string[] args)
        {
            //var twitterClient = new VismaTwitterClient();
            //var results = twitterClient.Search("a", "");

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel(options =>
                {
                    options.Listen(IPAddress.Loopback, 4000);
                })
                .Build();
    }
}
