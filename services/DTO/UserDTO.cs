﻿using System;
namespace services.DTO
{
    public class UserDTO
    {
        public UserDTO(string email) {
            this.Email = email;
        }

        public int UserId { get; private set; }
        public string Email { get; private set; }
    }
}
