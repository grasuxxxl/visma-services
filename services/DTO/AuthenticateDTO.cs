﻿using System;
namespace services.DTO
{
    public class AuthenticateDTO
    {
        public AuthenticateDTO()
        {
        }

        public string Password { get; private set; }
        public string Username { get; private set; }
    }
}
