﻿using System;
namespace services.DTO
{
    public class RecoverDTO
    {
        public RecoverDTO()
        {
        }

        public string Password { get; set; }
        public string Token { get; set; }
    }
}
