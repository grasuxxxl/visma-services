﻿namespace services.DTO
{
    public class VismaTweetsQueryDTO
    {
        public string Query { get; set; }
        public string MaxId { get; set; }
    }
}