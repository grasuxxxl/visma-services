﻿using System;
using System.Linq;

namespace services.Services
{
    public class RecoveryService : IRecoveryService
    {
        private readonly VismaContext context;

        public RecoveryService(VismaContext context)
        {
            this.context = context;
        }

        public void CreateRecovery(User user)
        {
            var recovery = new Recovery(user);
            context.Recoveries.Add(recovery);
            context.SaveChanges();
        }

        public Recovery findByToken(string token)
        {
            return context.Recoveries.First(recovery => recovery.Token == token);
        }

        public bool TokenExists(string token)
        {
            return context.Recoveries.Any(recovery => recovery.Token == token);
        }
    }
}
