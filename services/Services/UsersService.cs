﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace services.Services
{
    public class UsersService : IUsersService
    {
        private readonly VismaContext context;

        public UsersService(VismaContext context) 
        {
            this.context = context;
        }

        public User FindByEmail(string email)
        {
            return context.Users.First(user => user.Email == email);
        }

        public IEnumerable<User> GetAll() 
        {
            return context.Users.ToList();
        }

        public void UpdatePassword(int userId, string password)
        {
            var user = context.Users.First(u => u.UserId == userId);
            user.Password = password;
            context.SaveChanges();
        }
    }
}
