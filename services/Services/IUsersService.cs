﻿using System;
using System.Collections.Generic;

namespace services.Services
{
    public interface IUsersService
    {
        IEnumerable<User> GetAll();
		User FindByEmail(string email);
        void UpdatePassword(int userId, string password);
    }
}
