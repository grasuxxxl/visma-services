﻿namespace services.Services
{
    public interface IRecoveryService
    {
        void CreateRecovery(User user);
        bool TokenExists(string token);
        Recovery findByToken(string token);
    }
}