﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace services.Services
{
    public class OAuthToken {
        public OAuthToken(string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret) {
            this.ConsumerKey = consumerKey;
            this.ConsumerSecret = consumerSecret;
            this.AccessToken = accessToken;
            this.AccessTokenSecret = accessTokenSecret;
                
        }

        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string AccessToken { get; set; }
        public string AccessTokenSecret { get; set; }
    }

    public class VismaTwitterClient
    {
        private readonly OAuthToken oauthToken;
        public VismaTwitterClient()
        {
            oauthToken = new OAuthToken(
                "1qP4G9jSCXNr3yFFCCOpUrpH2",
                "8R0Gi17rGBC85XTokHvGKjvQ1TAKGklCdqrKw7RYmf8teEYSdP",
                "88983206-FWy5kmZDL75iQrTNFCkfVDvQDCSjzSGg0OxwEGf7W", 
                "tNZiw6PtpQfd4d3cuRMdt3gpE5kf89icVdZnsMmfZH7OY");
        }

        public string Search(string query, string maxId)
        {
            

            OAuth.OAuthMessageHandler _handler = new OAuth.OAuthMessageHandler(
                oauthToken.ConsumerKey,
                oauthToken.ConsumerSecret,
                oauthToken.AccessToken,
                oauthToken.AccessTokenSecret);

            HttpClient client = new HttpClient(_handler);

            var response = client.GetAsync($"https://api.twitter.com/1.1/search/tweets.json?max_id={maxId}&q={query}&result_type=recent").Result;

            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
