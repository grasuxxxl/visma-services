﻿using System;
using Microsoft.EntityFrameworkCore;

namespace services
{
    public class User
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class Recovery
    {
        public Recovery() {}

        public Recovery(User user) {
            this.Token = Guid.NewGuid().ToString();
            this.Date = DateTime.UtcNow;
            this.UserId = user.UserId;
            this.User = user;
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Token { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }

    public class VismaContext : DbContext
    {
        public VismaContext(DbContextOptions<VismaContext> options) : base(options) {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // optionsBuilder.UseNpgsql("Server=127.0.0.1;Port=5432;Database=work-tracker;User Id=work-tracker;Password=work-tracker;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");
                entity.Property(e => e.UserId).IsRequired().HasColumnName("id");
                entity.Property(e => e.Email).IsRequired().HasColumnName("email");
                entity.Property(e => e.Password).IsRequired().HasColumnName("password");
                entity.HasKey(e => e.UserId);
            });

            modelBuilder.Entity<Recovery>(entity => 
            {
                entity.ToTable("recoveries");
                entity.Property(e => e.Id).IsRequired().HasColumnName("id");
                entity.Property(e => e.Date).IsRequired().HasColumnName("date");
                entity.Property(e => e.Token).IsRequired().HasColumnName("token");
                entity.Property(e => e.UserId).IsRequired().HasColumnName("user_id");
                entity.HasKey(e => e.Id);
                entity.HasOne(e => e.User);
            });
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Recovery> Recoveries { get; set; }
    }


}
